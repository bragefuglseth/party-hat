# entry_row.py
#
# Copyright 2023 Party Hat Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import datetime

from gi.repository import Adw, Gtk

from .entry_view import EntryViewPage


@Gtk.Template(resource_path="/io/gitlab/gregorni/PartyHat/gtk/entry-row.ui")
class EntryRow(Adw.ActionRow):
    __gtype_name__ = "EntryRow"

    days_left_label = Gtk.Template.Child()

    def __init__(self, parent, data, **kwargs):
        super().__init__(**kwargs)

        self.parent = parent
        self.data = data

        self.set_title(data.get("name"))

        def get_days_until_birthday(birthday, today):
            days_until_birthday = (
                datetime.date(today.year, birthday.month, birthday.day) - today
            ).days

            return (
                days_until_birthday
                if days_until_birthday >= 0
                else (
                    datetime.date(today.year + 1, birthday.month, birthday.day) - today
                ).days
            )

        self.days_left = get_days_until_birthday(
            datetime.date.fromisoformat(data.get("birth-date")), datetime.date.today()
        )
        # Translators: Do not translate "{days_left}"!
        days_left_string = _("{days_left} days left").format(days_left=self.days_left)
        if self.days_left == 0:
            days_left_string = _("Today!")
        elif self.days_left == 1:
            days_left_string = _("Tomorrow")
        self.days_left_label.set_label(days_left_string)

        self.connect("activated", self.__view_entry)

    def __view_entry(self, *args):
        entry_view_nav_page = EntryViewPage(self, self.data)
        self.parent.navigation_view.push(entry_view_nav_page)
