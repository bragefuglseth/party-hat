# update_fav_btn.py
#
# Copyright 2023 Party Hat Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later


def update_fav_btn(button, *args):
    favourited = button.get_icon_name() == "starred-symbolic"
    button.add_css_class("flat")
    button.remove_css_class("favourited")
    button.set_icon_name("non-starred-symbolic")
    if not favourited:
        button.remove_css_class("flat")
        button.add_css_class("favourited")
        button.set_icon_name("starred-symbolic")
