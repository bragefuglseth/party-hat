# entry_view.py
#
# Copyright 2023 Party Hat Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import datetime

from dateutil import relativedelta
from gi.repository import Adw, Gtk

from . import update_fav_btn
from .entry_edit import EntryEditWindow


@Gtk.Template(resource_path="/io/gitlab/gregorni/PartyHat/gtk/entry-view.ui")
class EntryViewPage(Adw.NavigationPage):
    __gtype_name__ = "EntryViewPage"

    edit_btn = Gtk.Template.Child()
    favourite_btn = Gtk.Template.Child()
    person_icon = Gtk.Template.Child()
    name_label = Gtk.Template.Child()
    birth_date_row = Gtk.Template.Child()
    days_left_row = Gtk.Template.Child()
    current_age_row = Gtk.Template.Child()

    def __init__(self, parent, data, **kwargs):
        super().__init__(**kwargs)

        self.set_title(data.get("name"))

        self.name_label.set_label(data.get("name"))

        birth_date_str = data.get("birth-date")
        today = datetime.date.today()
        birth_date = datetime.date.fromisoformat(birth_date_str)

        self.birth_date_row.set_subtitle(birth_date_str)
        self.days_left_row.set_subtitle(f"{parent.days_left}")
        self.current_age_row.set_subtitle(
            f"{relativedelta.relativedelta(today, birth_date).years}"
        )

        if data.get("is-favourite"):
            update_fav_btn.update_fav_btn(self.favourite_btn)

        def update_favourite(*args):
            data["is-favourite"] = not data.get("is-favourite")
            update_fav_btn.update_fav_btn(self.favourite_btn)

        self.favourite_btn.connect("clicked", update_favourite)

        if parent.days_left == 0:
            self.person_icon.set_name("special-person")
