# entry_edit.py
#
# Copyright 2023 Party Hat Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw, Gtk

from . import update_fav_btn


@Gtk.Template(resource_path="/io/gitlab/gregorni/PartyHat/gtk/entry-edit.ui")
class EntryEditWindow(Adw.Window):
    __gtype_name__ = "EntryEditWindow"

    cancel_btn = Gtk.Template.Child()
    save_btn = Gtk.Template.Child()
    name_row = Gtk.Template.Child()
    birth_date_row = Gtk.Template.Child()
    birth_date_btn = Gtk.Template.Child()
    birth_date_cal = Gtk.Template.Child()
    favourite_btn = Gtk.Template.Child()

    def __init__(self, parent_window, finish_function, data=None, **kwargs):
        super().__init__(**kwargs)

        self.finish_function = finish_function

        self.set_transient_for(parent_window)

        self.favourite_btn.connect("clicked", update_fav_btn.update_fav_btn)
        self.cancel_btn.connect("clicked", self.__cancel)
        self.save_btn.connect("clicked", self.__save)
        self.name_row.connect("changed", self.__check_is_filled)
        self.birth_date_row.connect("activated", self.__open_calendar)
        self.birth_date_cal.connect("day-selected", self.__set_birth_date_row_subtitle)

        self.__set_birth_date_row_subtitle()

    def __open_calendar(self, *args):
        self.birth_date_btn.activate()

    def __check_is_filled(self, *args):
        self.save_btn.set_sensitive(self.name_row.get_text() != "")

    def __set_birth_date_row_subtitle(self, *args):
        self.birth_date_row.set_subtitle(
            self.birth_date_cal.get_date().format_iso8601()[:10]
        )

    def __cancel(self, *args):
        self.__close(None)

    def __save(self, *args):
        self.__close(
            {
                "name": self.name_row.get_text(),
                "birth-date": self.birth_date_row.get_subtitle(),
                "is-favourite": self.favourite_btn.get_icon_name()
                == "starred-symbolic",
            }
        )

    def __close(self, data):
        self.finish_function(data)
        self.destroy()
