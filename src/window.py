# window.py
#
# Copyright 2023 Party Hat Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw, Gio, Gtk

from .entry_edit import EntryEditWindow
from .entry_row import EntryRow


@Gtk.Template(resource_path="/io/gitlab/gregorni/PartyHat/gtk/window.ui")
class PartyHatWindow(Adw.ApplicationWindow):
    __gtype_name__ = "PartyHatWindow"

    first_page_stack = Gtk.Template.Child()
    people_list_preferences_group = Gtk.Template.Child()
    add_entry_btn_welcome = Gtk.Template.Child()
    add_entry_btn = Gtk.Template.Child()
    navigation_view = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        settings = Gio.Settings(schema_id="io.gitlab.gregorni.PartyHat")
        settings.bind("width", self, "default-width", Gio.SettingsBindFlags.DEFAULT)
        settings.bind("height", self, "default-height", Gio.SettingsBindFlags.DEFAULT)
        settings.bind("is-maximized", self, "maximized", Gio.SettingsBindFlags.DEFAULT)

        self.add_entry_btn_welcome.connect("clicked", self.on_new_entry)
        self.add_entry_btn.connect("clicked", self.on_new_entry)

    def on_new_entry(self, *args):
        edit_window = EntryEditWindow(
            parent_window=self, finish_function=self.__on_edit_entry_closed
        )
        edit_window.present()

    def __on_edit_entry_closed(self, data):
        if data:
            self.entry_data = data

            action_row = EntryRow(self, data)
            self.people_list_preferences_group.add(action_row)
            self.first_page_stack.set_visible_child_name("people-list")
